import { NgModule } from 'angular-ts-decorators';
import { AppComponent } from './app.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import bootstrap from 'angular-ui-bootstrap';

@NgModule({
  id: 'AppModule',
  imports: [
    bootstrap,
  ],
  declarations: [
    AppComponent,
    DatepickerComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {}
