import { Component } from 'angular-ts-decorators';
import { By, ComponentFixture, TestBed } from 'angularjs-testbed';
import { DatepickerComponent } from './datepicker.component';

describe('DatepickerComponent', () => {
  let component: DatepickerComponent;
  let fixture: ComponentFixture<DatepickerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DatepickerComponent,
      ],
    }).compileComponents();

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be correct format', () => {
    let format = component.format;
    expect(format).toEqual('dd-MMMM-yyyy');
  });

  it('should be correct date', () => {
    const date = new Date();
    const day = date.getDay();
    const month = date.getMonth();
    const year = date.getFullYear();

    expect(component.dt.getDay()).toEqual(day);
    expect(component.dt.getMonth()).toEqual(month);
    expect(component.dt.getFullYear()).toEqual(year);
  });

  it('should init popup', () => {
    expect(component.popup).toEqual({});
  });

  it('should handle click and show popup', () => {
    spyOn(component, 'open');
    let element = fixture.debugElement.nativeElement.querySelector('button');

    element.click();
    expect(component.open).toHaveBeenCalled();
    expect(component.popup).toBeTruthy();
  });

});
