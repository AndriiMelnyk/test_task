import { Component, OnInit } from 'angular-ts-decorators';

@Component({
  selector: 'datapicker',
  template: require('./datepicker.component.html')
})
export class DatepickerComponent implements OnInit {
  popup: any;
  dt = new Date();
  format: string = 'dd-MMMM-yyyy';

  ngOnInit() {
    this.popup = {};
  }

  open() {
    this.popup.opened = true;
  }
}
